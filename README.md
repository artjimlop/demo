# About the app

Hello! I'm a demo project with the next feature:
  - You can go from a users list to a user detail

Basically, I retrieve a list of Github users from an API and save them on a local DB!

# Basic Stack

  - MVVM + LiveData: Avoid a lot of boilerplate communication between in the presentation layer + lifecycle awareness
  - Coroutines
  - Retrofit
  - Room
  - Koin

# Tests

  - ViewModel tests with __mockk__
  - UserListToUserDetailTest is testing the navigation from user list to user detail using __Espresso + Page pattern__
  - Repository is also tested with mockk

About mockk: I've been using it for a while now and I love it. It can mock final classes and Kotlin objects out of the box and I find it has a better syntax for defining mocks

About Pages: I find it makes the tests readable, approaching them to how you would explain the business logic.

# Decisions

### Architecture

The concept behind the project is to ressemble as much as possible to the architecture of a real-life app in production. The app is modularized.

- app: as lightweight as possible.

- features
    - User list: the scope of this feature is just showing a list of users
    - User detail: the scope of this feature is just showing the user profile

- common
    - core: contains resources shared by features. An iteration over this (to avoid creating a miscelanea module) would be creating specific core modules for each feature. Example: core-users.
    - core-testing: contains needed resources for testing. In this case, only an abstraction to avoid boilerplate in unit tests.
    - navigation: responsible of the navigation, exposes an interface which will be implemented by an specific navigator.

package com.example.userdetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.userdetail.viewmodel.UserDetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserDetailActivity : AppCompatActivity() {

    private val userViewModel by viewModel<UserDetailViewModel>()

    private val id: Long
        get() = intent.extras?.getLong(ID) ?: throw IllegalArgumentException("The ID parameter can not be null.")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        userViewModel.loadUser(id).observe(this, Observer {
            it?.let {
                login.text = it.login
                Picasso.get().load(it.avatar_url).into(avatar)
            }
        })
    }
}
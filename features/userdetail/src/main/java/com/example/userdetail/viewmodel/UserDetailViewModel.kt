package com.example.userdetail.viewmodel

import androidx.lifecycle.ViewModel
import com.example.core.UserRepository

class UserDetailViewModel(private val userRepository: UserRepository) : ViewModel() {

    fun loadUser(id: Long) = userRepository.getUser(id)
}
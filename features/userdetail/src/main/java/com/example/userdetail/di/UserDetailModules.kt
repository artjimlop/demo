package com.example.userdetail.di

val userDetailModules =
    listOf(
        userDetailNavigationModule,
        viewModelModule
    )
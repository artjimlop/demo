package com.example.userlist.model.repository

import com.example.core.UserRepository
import com.example.userlist.model.api.UserApi
import com.example.userlist.model.dao.UserDao

class UserRepositoryImpl(private val userApi: UserApi, private val userDao: UserDao): UserRepository {

    override val data = userDao.findAll()

    override suspend fun refresh() {
        val users = userApi.getAllAsync()
        userDao.add(users)
    }

    override fun getUser(id: Long) = userDao.findUser(id)
}
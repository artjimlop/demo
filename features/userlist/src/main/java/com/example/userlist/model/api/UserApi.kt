package com.example.userlist.model.api

import com.example.core.entity.GithubUser
import retrofit2.http.GET

interface UserApi {

    @GET("users")
    suspend fun getAllAsync(): List<GithubUser>
}
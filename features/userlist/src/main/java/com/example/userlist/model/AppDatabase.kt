package com.example.userlist.model

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.userlist.model.dao.UserDao
import com.example.core.entity.GithubUser

@Database(entities = [GithubUser::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val userDao: UserDao
}
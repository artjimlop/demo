package com.example.userlist.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.navigation.UserDetailNavigator
import com.example.userlist.R
import com.example.userlist.ui.adapters.UserListAdapter
import com.example.userlist.utils.LoadingState
import com.example.userlist.viewmodel.UserListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val userViewModel by viewModel<UserListViewModel>()
    private val userDetailNavigator: UserDetailNavigator by inject()
    private val userListAdapter: UserListAdapter =
        UserListAdapter { user -> userDetailNavigator.goToUserDetail(this@MainActivity, user.id) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = userListAdapter
        }

        userViewModel.data.observe(this, Observer {
            if (it.isNotEmpty()) {
                userList.visibility = View.VISIBLE
                userListAdapter.updateUsers(it)
            }
        })

        userViewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> Toast.makeText(
                    baseContext,
                    it.msg,
                    Toast.LENGTH_SHORT
                ).show()
                LoadingState.Status.RUNNING -> Toast.makeText(
                    baseContext,
                    "Loading",
                    Toast.LENGTH_SHORT
                ).show()
                LoadingState.Status.SUCCESS -> Toast.makeText(
                    baseContext,
                    "Success",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}

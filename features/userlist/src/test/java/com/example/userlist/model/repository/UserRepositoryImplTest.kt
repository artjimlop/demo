package com.example.userlist.model.repository

import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import com.example.userlist.model.api.UserApi
import com.example.userlist.model.dao.UserDao
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyOrder
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class UserRepositoryImplTest {

    private lateinit var repository: UserRepository
    private val userDao: UserDao = mockk(relaxed = true)
    private val userApi: UserApi = mockk()
    private val user = GithubUser(ID, "login", "avatar_url")

    @Before
    fun setUp() {
        repository = UserRepositoryImpl(userApi, userDao)
    }

    @Test
    fun `WHEN refreshing users from API, THEN add them to DB`() = runBlocking {
        val users = listOf(user)
        coEvery { userApi.getAllAsync() } answers { users }

        repository.refresh()

        verifyOrder {
            runBlocking { userApi.getAllAsync() }
            userDao.add(users)
        }
    }


    @Test
    fun `WHEN getting user by id, THEN find it on the DB`() = runBlocking {
        repository.getUser(ID)

        verify {
            userDao.findUser(ID)
        }
    }

    @Test
    fun `WHEN loading data, THEN load it from the DB`() = runBlocking {
        repository.data

        verify {
            userDao.findAll()
        }
    }
}

private const val ID = 0L
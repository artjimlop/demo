package com.example.userlist

import androidx.lifecycle.MutableLiveData
import com.example.core.UserRepository
import com.example.core.entity.GithubUser
import com.example.userlist.viewmodel.UserListViewModel
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Test

@ExperimentalCoroutinesApi
class UserListViewModelTest: com.example.core_testing.BaseViewModelTest() {
    private lateinit var viewModel: UserListViewModel
    private val userRepository: UserRepository = mockk(relaxed = true, relaxUnitFun = true)
    private val users: MutableLiveData<List<GithubUser>> by lazy {
        MutableLiveData<List<GithubUser>>()
    }

    override fun onPrepareTest() {
        viewModel = UserListViewModel(userRepository)
        coEvery { userRepository.data } answers  { users }
    }

    @Test
    fun `WHEN viewmodel is initialized, THEN data should be refreshed`() = runBlocking {
        userRepository.refresh()
    }

    @Test
    fun `WHEN data is called, THEN it should be retrieved from the repository`() = runBlocking {
        viewModel.data
        verify { userRepository.data }
    }
}
package com.example.navigation

import android.content.Context

interface UserDetailNavigator {

    fun goToUserDetail(context: Context, id: Long)
}
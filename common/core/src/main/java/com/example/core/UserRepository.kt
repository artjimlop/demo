package com.example.core

import androidx.lifecycle.LiveData
import com.example.core.entity.GithubUser

interface UserRepository {
    suspend fun refresh()
    val data: LiveData<List<GithubUser>>
    fun getUser(id: Long): LiveData<GithubUser>
}
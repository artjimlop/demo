package com.demoproject.mobile.androidtest.app

import com.demoproject.mobile.androidtest.di.testModules
import com.example.userdetail.di.userDetailModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class TestApp: App() {

    override fun startKoin() {
        startKoin {
            androidContext(this@TestApp)
            androidLogger(Level.DEBUG)
            modules(testModules)
            modules(userDetailModules)
        }
    }
}
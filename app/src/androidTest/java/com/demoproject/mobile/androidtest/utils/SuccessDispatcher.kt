package com.demoproject.mobile.androidtest.utils

import com.demoproject.mobile.androidtest.utils.FileReader.readStringFromFile
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

class SuccessDispatcher : Dispatcher() {

    override fun dispatch(request: RecordedRequest): MockResponse {
        return MockResponse()
            .setResponseCode(200)
            .setBody(readStringFromFile("success_response.json"))
    }
}
package com.demoproject.mobile.androidtest.pages

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.demoproject.mobile.androidtest.R

class DetailPage {
    fun checkUser() {
        onView(withId(R.id.login))
            .check(matches(withText("mojombo")))
    }
}
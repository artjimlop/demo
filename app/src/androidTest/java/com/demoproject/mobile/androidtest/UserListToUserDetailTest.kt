package com.demoproject.mobile.androidtest

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.demoproject.mobile.androidtest.pages.DetailPage
import com.demoproject.mobile.androidtest.pages.MainPage
import com.demoproject.mobile.androidtest.utils.SuccessDispatcher
import com.example.userlist.ui.MainActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class UserListToUserDetailTest {
    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java, true, false)

    private val mockWebServer = MockWebServer()
    private val mainPage = MainPage()
    private val detailPage = DetailPage()

    @Before
    fun init() {
        mockWebServer.start(8080)
        mockWebServer.dispatcher = SuccessDispatcher()
        activityTestRule.launchActivity(null)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testSuccessfulResponse() {
        mainPage.clickOnFirstUser()
        detailPage.checkUser()
    }
}
